package com.vub.lab4;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Location {

    private String name;

    @Override
    public String toString() {
        return name;
    }
}
